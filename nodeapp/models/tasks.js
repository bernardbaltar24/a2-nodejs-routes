//Declare Dependencies

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Define your schema
//description, teamId-String, isCompleted-boolean
const taskSchema = new Schema(
	{
		description:String,
		teamId: String,
		isCompleted: Boolean
	},
	{
		timestamps: true
	}
	);

//Export your model

module.exports = mongoose.model("Task", taskSchema);